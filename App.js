import 'react-native-gesture-handler';
import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Login from './Auth/Login';
import Register from './Auth/Register';
import Home from './Auth/Home';



const App: () => React$Node = () => {
  return(
    <>
      <Home />
      </>
//     <View style={{paddingTop: 200}} >
//         <Text style={{fontSize: 18, fontWeight: 'bold', color: '#33ffda', paddingHorizontal: 50}}>
//             Selamat Datang Pada Home Aplikasi Ini
//         </Text>
//         <RedirectButton />
//     </View>
// );
// };
  
//   const RedirectButton = () => {
//     return(
//        <>
//        <View>
//             <Text style={{fontSize: 18, color: '#33ffda', padding: 50, paddingStart: 100}}>
//                 Silahkan Pilih Menu Dibawah Ini
//             </Text>
//             <TouchableOpacity style={{alignItems: 'center'}}>
//             <Text style={{padding:20}}>Login</Text>
//             </TouchableOpacity>
//             <TouchableOpacity style={{alignItems: 'center'}}>
//             <Text style={{padding:20}}>Register</Text>
//             </TouchableOpacity>
//         </View>

//     </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
