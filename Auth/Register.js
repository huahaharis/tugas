import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, Button, ImageBackground, ScrollView, SafeAreaView } from 'react-native';
import BgImage from '../Gambar/bg.jpeg';
import Icon from 'react-native-vector-icons';


class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      showPass: true,
      press: false
    }
  }
  onPressSumbit = () => {
    const { email, password, confirmPassword } = this.state;
    if (email.length <= 0) {
      alert("Please fill out the email required fields.")
    } else if (password.length <= 0) {
      alert("Please fill out the password required fields."); 
    } else if (password != confirmPassword){
      alert("Please correct your confirm password!!")
    } 
  };

  showPass = () => {
      if (this.state.press == false) {
          this.setState({ showPass: false, press: true})
      } else {
          this.setState({ showPass: true, press: false})
      }
  }
  
  render() {
    return (
      <>
        {/* <ImageBackground source={BgImage} style={styles.backgroundContainer}> */}
        <ScrollView>
            <View style={styles.container}>
            <Text style={{ fontSize: 32, color: 'black', padding: 60 }}>REGISTER</Text>
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ email: text })}
              value={this.state.email}
              editable={true}
              maxLength={25}
              multiline={false}
              placeholder="Masukkan Email"
            />
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ password: text })}
              value={this.state.password}
              editable={true}
              secureTextEntry={this.state.showPass}
              maxLength={40}
              multiline={false}
              placeholder="Masukkan Password"
            />
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ confirmPassword: text })}
              value={this.state.confirmPassword}
              editable={true}
              secureTextEntry={this.state.showPass}
              maxLength={40}
              multiline={false}
              placeholder="Masukkan Ulang Password"
            />
            <View style={styles.buttonStyle}>
              <Button
                containerStyle={styles.submitContainer}
                onPress={() => this.onPressSumbit()}
                title="SUMBIT"
                color='black'/>
            </View>
          </View>
          </ScrollView>
      </>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 120,
    height: 700,
    alignItems: 'center',
    backgroundColor: '#44bcd8'
  },
  logo: {
    width: '100%',
    height: 200
  },
  inputField: {
    width: 300,
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  redirectLink: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  link: {
    color: 'blue'
  },
  submitContainer: {
    width: 55,
    backgroundColor: '#cce7e8',
    borderRadius: 50,
    height: 45,
    marginTop: 20,
    justifyContent: 'center'
  },
  validationErrors: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  error: {
    marginTop: 10,
    textAlign: 'center',
    color: 'red'
  },
  buttonStyle: {
    marginTop: 15,
    flex: 1,
    width: 300,
    borderRadius: 60/2,
    color:'#cce7e8'
  },
  copyright: {
    marginTop: 15,
    flex: 1,
    alignSelf: 'center'
  },
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Register;