import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, Button, TouchableOpacity, ScrollView } from 'react-native';
import BgImage from '../Gambar/bg.jpeg';


class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',

    }
  }
  onPressLogin = () => {
    const { email, password } = this.state;
    if (email.length <= 0) {
      alert("Please fill out the email required fields.")
    } else if (password.length <= 0) {
      alert("Please fill out the password required fields."); 
    }
  }

  
  render() {
    return (
      <>
        {/* <ImageBackground source={BgImage} style={styles.backgroundContainer}> */}
          
        <ScrollView>
            <View style={styles.container}>
            <Text style={{ fontSize: 32, color: 'black', padding: 70 }}>LOGIN</Text>
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ email: text })}
              value={this.state.email}
              editable={true}
              maxLength={40}
              multiline={false}
              placeholder="Masukkan Email"
            />
            <TextInput
              style={styles.inputField}
              onChangeText={text => this.setState({ password: text })}
              value={this.state.password}
              editable={true}
              secureTextEntry={true}
              maxLength={40}
              multiline={false}
              placeholder="Masukkan Password"
            />
            <View style={styles.buttonStyle}>
              <Button
                containerStyle={styles.loginContainer}
                onPress={() => this.onPressLogin()}
                title="LOG IN"
                color='black'
                />
            </View>
            <View style={styles.redirectLink}>
                        <Text style={{color:'black'}}>Tidak punya akun? </Text>
                        <TouchableOpacity onPress={() =>
                        this.props.navigation.navigate('Register')}>
                        <Text style={styles.link}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
          </View>
          </ScrollView>
        {/* </ImageBackground> */}
      </>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 700,
    paddingTop: 120,
    alignItems: 'center',
    backgroundColor: '#44bcd8'
  },
  logo: {
    width: '100%',
    height: 200
  },
  inputField: {
    width: 300,
    height: 43,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  redirectLink: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center'
  },
  redirectLink: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    paddingBottom: 200
  },
  link: {
    color: 'blue'
  },
  loginContainer: {
    width: 55,
    backgroundColor: 'black',
    borderRadius: 25,
    height: 45,
    marginTop: 20,
    justifyContent: 'center'
  },
  validationErrors: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  error: {
    marginTop: 10,
    textAlign: 'center',
    color: 'red'
  },
  buttonStyle: {
    marginTop: 15,
    flex: 1,
    width: 300,
    color: 'black'
  },
  copyright: {
    marginTop: 15,
    flex: 1,
    alignSelf: 'center'
  },
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Login;