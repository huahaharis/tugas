import 'react-native-gesture-handler';
import React from 'react';
import { Card } from 'react-native-paper';
import {View, Text, TouchableOpacity, Button, StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Register from './Register';
import Login from './Login';
import { ScrollView } from 'react-native-gesture-handler';


// const RedirectButton = () => {
//     return(
//         <View>
//             <Text style={{fontSize: 18, color: '#33ffda', padding: 50, paddingStart: 100}}>
//                 Silahkan Pilih Menu Dibawah Ini
//             </Text>
//             <Button
//                 title='Login'
//                 onPress={() => NavigationServices.navigate(Login)}/>
//             <TouchableOpacity /*onPress={() => navigate('Register')}*/ style={{alignItems: 'center'}}>
//             <Text style={{padding:20}}>Register</Text>
//             </TouchableOpacity>
//         </View>
//     )
// }
// const Home = () => {
//     return(
        
//         <View style={{paddingTop: 200}} >
//             <NavStack 
//             ref={navigatorRef => {
//                 NavigationServices.setTopLevelNavigation(navigatorRef);
//             }}/>
//             <Text style={{fontSize: 18, fontWeight: 'bold', color: '#33ffda', paddingHorizontal: 50}}>
//                 Selamat Datang Pada Home Aplikasi Ini
//             </Text>
//         </View>
//     );
// };
function Home(props) {
const {navigation} = props;
    return(
        <>
        <ScrollView>
        <View style={{flex:1}}>
        <View >
            <Card >
                <Card.Cover source={require('../Gambar/fb.jpg')} />
            </Card>
        </View>
        <View style={{alignItems: 'center', justifyContent: 'center', paddingBottom:100, backgroundColor: '#44bcd8', height:490}}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>Selamat Datang Di Facebook</Text>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>apabila sudah memiliki akun silahkan login</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Login')} style={{alignItems: 'center'}}>
            <Text style={{padding:20, fontSize: 22, color: 'black'}}>Login</Text>
            </TouchableOpacity>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: 'black'}}>apabila belum memiliki akun silahkan register</Text>
            <TouchableOpacity onPress={() => navigation.navigate('Register')} style={{alignItems: 'center'}}>
            <Text style={{padding:20, fontSize: 20, color: 'black'}}>Register</Text>
            </TouchableOpacity>
        </View>
        </View>
        </ScrollView>
        </>
    )
}

// const AppStack = createStackNavigator({
//     Login: Login,
//     Register: Register
// });

// const AuthNavigator = createStackNavigator({
//     HomeRoute: {
//         screen: Home,
//         navigationOptions: () => ({
//             header: null
//         })
//     }
// });

const Stack = createStackNavigator();

export default () => (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home} style={{color: 'black'}}/>
                <Stack.Screen name="Login" component={Login}/>
                <Stack.Screen name="Register" component={Register}/>
            </Stack.Navigator>
        </NavigationContainer>
);